import React from 'react';
import { addDecorator, storiesOf, action } from '@kadira/storybook';
import { muiTheme } from 'storybook-addon-material-ui';
import { Provider } from 'mobx-react';

import AppStore from '../src/stores/AppStore';
import PracticeTestStore from '../src/stores/PracticeTestStore';
import QuestionStore from '../src/stores/QuestionStore';
import TestStore from '../src/stores/TestStore';

import '../src/styles/base/_reset.scss';
import '../src/styles/base/_base.scss';

import AddQuestionForm from '../src/components/Tests/AddQuestionForm/AddQuestionForm';
import LoginContainer from '../src/components/Login/LoginContainer';
import TopBar from '../src/components/App/TopBar/TopBar';
import TestsContainer from '../src/components/Tests/TestsContainer';
import Vote from '../src/components/Vote/Vote';

const appStore = new AppStore();
const practiceTestStore = new PracticeTestStore();
const questionStore = new QuestionStore();
const testStore = new TestStore();

const store = {
  appStore, practiceTestStore, questionStore, testStore
};

addDecorator(muiTheme());
addDecorator((story) => (
  <Provider store={store}>
    {story()}
  </Provider>
));

const renderWithPadding = (elem) => {
  return (
    <div style={{ padding: '2rem' }}>
      {elem}
    </div>
  );
};

storiesOf('AddQuestionForm', module)
  .add('default', () => (
    <AddQuestionForm
      handleClose={action('clicked handleClose')}
      handleSubmit={action('clicked handleSubmit')}
      isOpen
    />
  ));

storiesOf('LoginContainer', module)
  .add('default', () => (
    <LoginContainer />
  ));

storiesOf('TestsContainer', module)
  .add('default', () => (
    <TestsContainer />
  ));

storiesOf('TopBar', module)
  .add('default', () => (
    renderWithPadding(<TopBar />)
  ));

storiesOf('Vote', module)
  .add('default', () => (
    renderWithPadding(
      <Vote upvote={() => {}} downvote={() => {}} currentScore={23} />
    )
  ));
