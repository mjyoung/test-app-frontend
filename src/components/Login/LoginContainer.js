import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router-dom';
import { action, observable } from 'mobx';
import { inject, observer } from 'mobx-react';

import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';

import styles from './LoginContainer.scss';

@inject('store') @observer
export default class LoginContainer extends Component {
  @observable email = '';
  @observable password = '';

  @action handleEmailChange = (event, value) => {
    this.email = value;
  }

  @action handlePasswordChange = (event, value) => {
    this.password = value;
  }

  handleLogin = () => {
    this.props.store.appStore.login(this.email, this.password);
  }

  handleLoginWithFacebook = () => {
    this.props.store.appStore.loginWithFacebook();
  }

  render() {
    return (
      <div className={styles.login}>
        <TextField
          value={this.email}
          floatingLabelText="Email"
          onChange={this.handleEmailChange}
        />
        <TextField
          value={this.password}
          floatingLabelText="Password"
          type="password"
          onChange={this.handlePasswordChange}
        />
        <RaisedButton
          className={styles.loginButton}
          label="Login"
          primary
          onTouchTap={this.handleLogin}
        />
        <RaisedButton
          className={styles.loginButton}
          label="Login with Facebook"
          primary
          onTouchTap={this.handleLoginWithFacebook}
        />
        <div>
          Not yet registered? <Link to="/register">Sign up</Link> for a free account!
        </div>
      </div>
    );
  }
}

LoginContainer.wrappedComponent.propTypes = {
  store: PropTypes.shape().isRequired
};
