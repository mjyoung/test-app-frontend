import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router-dom';
import { inject, observer } from 'mobx-react';

import styles from './TestsContainer.scss';

@inject('store') @observer
export default class TestsContainer extends Component {
  componentWillMount() {
    this.props.store.testStore.fetchTests();
  }

  render() {
    const tests = this.props.store.testStore.tests.map((test) => {
      return (
        <div key={test.name}>
          <Link to={`/tests/${test.id}/${test.kebabName}`}>{test.name}</Link>
        </div>
      );
    });
    return (
      <div className={styles.tests}>
        Tests
        {tests}
      </div>
    );
  }
}

TestsContainer.wrappedComponent.propTypes = {
  store: PropTypes.shape().isRequired
};
