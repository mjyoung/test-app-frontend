import React, { Component, PropTypes } from 'react';
import { renderRoutes } from 'react-router-config';
import { inject, observer } from 'mobx-react';
import RaisedButton from 'material-ui/RaisedButton';

import styles from './PracticeTestContainer.scss';

@inject('store') @observer
export default class PracticeTestContainer extends Component {
  async componentWillMount() {
    const { testId } = this.props.match.params;
    await this.props.store.testStore.fetchSingleTest(testId);
    await this.props.store.practiceTestStore.fetchPracticeQuestionsForTest(testId);
  }

  handleStartTest = () => {
    const { testId } = this.props.match.params;
    const { selectedTest } = this.props.store.testStore;
    this.props.history.push(`/tests/${testId}/${selectedTest.kebabName}/practice/questions/1`);
  }

  render() {
    const { route } = this.props;
    const { selectedTest } = this.props.store.testStore;
    const { loadingPracticeQuestions, practiceQuestionIds } = this.props.store.practiceTestStore;
    if (loadingPracticeQuestions) {
      return (
        <div>
          Loading...
        </div>
      );
    }

    if (!loadingPracticeQuestions && practiceQuestionIds.length === 0) {
      return (
        <div className={styles.practiceTest}>
          <h1>Selected Test: {selectedTest.name}</h1>
          <div>
            There are not enough questions to take a practice test.
          </div>
          {renderRoutes(route.routes)}
        </div>
      );
    }

    return (
      <div className={styles.practiceTest}>
        <h1>Selected Test: {selectedTest.name}</h1>
        <div>
          <RaisedButton label="Start the Test!" onTouchTap={this.handleStartTest} />
        </div>
        {renderRoutes(route.routes)}
      </div>
    );
  }
}

PracticeTestContainer.wrappedComponent.propTypes = {
  history: PropTypes.shape().isRequired,
  match: PropTypes.shape().isRequired,
  route: PropTypes.shape().isRequired,
  store: PropTypes.shape().isRequired
};
