import React from 'react';
import ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import { BrowserRouter } from 'react-router-dom';
import { renderRoutes } from 'react-router-config';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import './utils/tap-event-plugin';
import routes from './routes';

import './styles/base/_reset.scss';
import './styles/base/_base.scss';

ReactDOM.render(
  <AppContainer>
    <MuiThemeProvider>
      <BrowserRouter>
        {renderRoutes(routes)}
      </BrowserRouter>
    </MuiThemeProvider>
  </AppContainer>,
  document.getElementById('root')
);

// Hot Module Replacement API
if (module.hot) {
  module.hot.accept(() => {
    ReactDOM.render(
      <AppContainer>
        <MuiThemeProvider>
          <BrowserRouter>
            {renderRoutes(routes)}
          </BrowserRouter>
        </MuiThemeProvider>
      </AppContainer>,
      document.getElementById('root')
    );
  });
}
