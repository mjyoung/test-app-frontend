import { isEmpty as _isEmpty } from 'lodash';
import { observable, action } from 'mobx';
import axios from '../utils/axios';

class TestStore {
  @observable tests = [];
  @observable selectedTest = null;

  fetchTests = async () => {
    const { data } = await axios.get('/tests');
    if (!_isEmpty(data)) { this.setTests(data); }
  }

  fetchSingleTest = async (id) => {
    const { data } = await axios.get(`/tests/${id}`);
    if (!_isEmpty(data)) { this.setSelectedTest(data); }
  }

  @action setTests = (data) => {
    this.tests = data;
  }

  @action setSelectedTest = (data) => {
    this.selectedTest = data;
  }
}

export default TestStore;
