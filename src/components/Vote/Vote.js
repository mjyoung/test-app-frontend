import React, { Component, PropTypes } from 'react';
import { observer } from 'mobx-react';

import styles from './Vote.scss';

@observer
export default class Vote extends Component {
  render() {
    const { downvote, upvote, currentScore } = this.props;
    return (
      <div className={styles.vote}>
        <button onClick={upvote}>
          <i className={`${styles.voteIcon} ${styles.upvote} icon-up-open`} />
        </button>
        {currentScore}
        <button onClick={downvote}>
          <i className={`${styles.voteIcon} ${styles.downvote} icon-down-open`} />
        </button>
      </div>
    );
  }
}

Vote.propTypes = {
  upvote: PropTypes.func.isRequired,
  downvote: PropTypes.func.isRequired,
  currentScore: PropTypes.number.isRequired
};
