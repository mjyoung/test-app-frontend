const webpack = require('webpack');

// you can use this file to add your custom webpack plugins, loaders and anything you like.
// This is just the basic way to add addional webpack configurations.
// For more information refer the docs: https://getstorybook.io/docs/configurations/custom-webpack-config

// IMPORTANT
// When you add this file, we won't add the default configurations which is similar
// to "React Create App". This only has babel loader to load JavaScript.

const loaders = [
  {
    test: /\.js$/,
    exclude: /node_modules/,
    loader: 'babel-loader',
    query: {
      presets: [
        ['es2015', { modules: false }],
        'stage-0',
        'react'
      ],
      plugins: [
        'transform-async-to-generator',
        'transform-decorators-legacy'
      ]
    }
  },
  {
    test: /\.js$/,
    exclude: /node_modules/,
    loader: 'eslint-loader'
  },
  {
    test: /\.scss|css$/,
    loader: 'style-loader!css-loader?modules&localIdentName=[local]___[hash:base64:5]!postcss-loader!resolve-url-loader!sass-loader?sourceMap'
  },
  {
    test: /\.(jpe?g|png|gif|svg)$/i,
    loader: 'file-loader?hash=sha512&digest=hex&name=assets/[hash].[ext]!image-webpack-loader?bypassOnDebug&optimizationLevel=7&interlaced=false'
  },
  {
    test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
    loader: 'url-loader?limit=10000&mimetype=application/font-woff'
  },
  {
    test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
    loader: 'file-loader'
  }
];

module.exports = {
  plugins: [],
  module: {
    loaders: loaders
  },
  resolve: { // https://github.com/storybooks/react-storybook/issues/485#issuecomment-249401443
    alias: {
      // this is used by the custom `react-router.js` module
      'react-router-original': require.resolve('react-router'),
      // this `react-router.js` will replace the Link with a our own mock component
      'react-router-dom': require.resolve('./react-router.js'),
    },
  },
};
