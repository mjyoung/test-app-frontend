import React, { Component, PropTypes } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import { filter as _filter } from 'lodash';

import Dialog from 'material-ui/Dialog';
import Checkbox from 'material-ui/Checkbox';
import FlatButton from 'material-ui/FlatButton';
import IconDelete from 'material-ui/svg-icons/action/delete';
import { List, ListItem } from 'material-ui/List';
import TextField from 'material-ui/TextField';

import styles from './AddQuestionForm.scss';

@observer
export default class AddQuestionForm extends Component {

  @observable answers = [];
  @observable newAnswerText = '';
  @observable newAnswerCorrect = false;
  @observable newQuestionText = '';

  @action handleNewAnswerAdd = () => {
    this.answers.push({
      text: this.newAnswerText,
      correct: this.newAnswerCorrect
    });
    this.newAnswerText = '';
    this.newAnswerCorrect = false;
  }

  @action handleNewAnswerRemove = (answerToRemove) => {
    const filteredAnswers = _filter(this.answers, (answer) => {
      return answer.text !== answerToRemove.text;
    });
    this.answers = filteredAnswers;
  }

  @action handleNewAnswerTextFieldChange = (event, value) => {
    this.newAnswerText = value;
  }

  @action handleNewAnswerCheckboxChange = (event, checked) => {
    this.newAnswerCorrect = checked;
  }

  @action handleNewQuestionTextFieldChange = (event, value) => {
    this.newQuestionText = value;
  }

  render() {
    const actions = [
      <FlatButton
        label="Cancel"
        primary
        onTouchTap={this.props.handleClose}
      />,
      <FlatButton
        label="Submit"
        secondary
        onTouchTap={() => {
          this.props.handleSubmit({
            question: this.newQuestionText,
            answers: this.answers
          });
        }}
      />
    ];
    return (
      <div className={styles.addQuestionForm}>
        <Dialog
          title="Add a Question"
          actions={actions}
          modal={false}
          open={this.props.isOpen}
          onRequestClose={this.props.handleClose}
          autoScrollBodyContent
        >
          <h2>New Question:</h2>
          <TextField
            hintText="Type the question here."
            floatingLabelText="New Question"
            fullWidth
            multiLine
            rows={1}
            rowsMax={5}
            value={this.newQuestionText}
            onChange={this.handleNewQuestionTextFieldChange}
          />
          <h2>Potential Answers:</h2>
          <div>
            <TextField
              hintText="Type a potential answer here."
              floatingLabelText="Add an answer (correct or incorrect)"
              fullWidth
              multiLine
              rows={1}
              rowsMax={5}
              value={this.newAnswerText}
              onChange={this.handleNewAnswerTextFieldChange}
            />
            <Checkbox
              label="Check if this answer is correct"
              checked={this.newAnswerCorrect}
              onCheck={this.handleNewAnswerCheckboxChange}
            />
            <FlatButton
              label="Add answer"
              primary
              onTouchTap={this.handleNewAnswerAdd}
              disabled={!this.newAnswerText}
            />
          </div>
          <h2>Added Answers:</h2>
          <div>
            <List>
              {this.answers.map((answer) => (
                <ListItem
                  key={answer.text}
                  className={answer.correct ? `${styles.answerItem} ${styles.answerCorrect}` : styles.answerItem}
                  disabled
                  primaryText={answer.text}
                  rightIcon={
                    <IconDelete onTouchTap={() => { this.handleNewAnswerRemove(answer); }} />
                  }
                />
              ))}
            </List>
          </div>
        </Dialog>
      </div>
    );
  }
}

AddQuestionForm.propTypes = {
  handleClose: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired
};
