import { isEmpty as _isEmpty } from 'lodash';
import { observable, action } from 'mobx';
import axios from '../utils/axios';

class PracticeTestStore {
  @observable loadingPracticeQuestions = true;
  @observable practiceQuestionIds = [];
  @observable practiceQuestions = [];

  fetchPracticeQuestionsForTest = async (testId) => {
    this.setLoadingPracticeQuestions(true);
    this.resetPracticeQuestionIds();
    this.resetPracticeQuestions();
    try {
      const { data } = await axios.get(`/tests/${testId}/practice/questions`);
      if (!_isEmpty(data)) {
        this.setPracticeQuestionIds(data.practiceQuestions.questions);
      }
      this.setLoadingPracticeQuestions(false);
    } catch (e) {
      this.setLoadingPracticeQuestions(false);
    }
  }

  fetchSingleQuestionForPracticeTest = async (testId, questionId) => {
    const { data } = await axios.get(`/tests/${testId}/questions/${questionId}`);
    if (!_isEmpty(data)) { this.updatePracticeQuestions(data); }
  }

  postPracticeTest = async (testId, answeredQuestions) => {
    await axios.post(`/tests/${testId}/practice`, {
      answeredQuestions
    });
  }

  @action setPracticeQuestionIds = (data) => {
    this.practiceQuestionIds = data;
  }

  @action resetPracticeQuestionIds = () => {
    this.practiceQuestionIds = [];
  }

  @action resetPracticeQuestions = () => {
    this.practiceQuestions = [];
  }

  @action updatePracticeQuestions = (data) => {
    this.practiceQuestions.push(data);
  }

  @action setLoadingPracticeQuestions = (bool) => {
    this.loadingPracticeQuestions = bool;
  }
}

export default PracticeTestStore;
