import { isEmpty as _isEmpty } from 'lodash';
import { observable, action } from 'mobx';
import axios from '../utils/axios';

class QuestionStore {
  @observable selectedQuestion = null;

  fetchSingleQuestionForTest = async (testId, questionId) => {
    const { data } = await axios.get(`/tests/${testId}/questions/${questionId}`);
    if (!_isEmpty(data)) { this.setSelectedQuestion(data); }
  }

  postQuestionForTest = (id, { question, answers }) => {
    axios.post(`/tests/${id}/questions`, {
      question, answers
    });
  }

  postVoteForQuestion = async (testId, questionId, vote) => {
    await axios.post(`/tests/${testId}/questions/${questionId}/votes`, {
      vote
    });
    this.fetchSingleQuestionForTest(testId, questionId);
  }

  @action setSelectedQuestion = (data) => {
    this.selectedQuestion = data;
  }
}

export default QuestionStore;
