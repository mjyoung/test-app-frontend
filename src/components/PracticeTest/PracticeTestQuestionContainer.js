import React, { Component, PropTypes } from 'react';
import { inject, observer } from 'mobx-react';
import { action, observable } from 'mobx';
import {
  cloneDeep as _cloneDeep,
  isEmpty as _isEmpty,
  find as _find,
  get as _get,
  map as _map
} from 'lodash';

import Checkbox from 'material-ui/Checkbox';
import RaisedButton from 'material-ui/RaisedButton';

import styles from './PracticeTestQuestionContainer.scss';

@inject('store') @observer
export default class PracticeTestQuestionContainer extends Component {
  componentWillMount() {
    const { testId } = this.props.match.params;
    const { fetchSingleQuestionForPracticeTest } = this.props.store.practiceTestStore;
    const questionIdToFind = this.getQuestionIdToFind(this.props);
    fetchSingleQuestionForPracticeTest(testId, questionIdToFind);
  }

  componentWillUpdate(nextProps) {
    const { testId } = nextProps.match.params;
    const {
      fetchSingleQuestionForPracticeTest,
      practiceQuestions
    } = nextProps.store.practiceTestStore;
    const questionIdToFind = this.getQuestionIdToFind(nextProps);
    const currentQuestion = _find(practiceQuestions, (question => {
      return question.id === questionIdToFind;
    }));
    if (_isEmpty(currentQuestion)) {
      fetchSingleQuestionForPracticeTest(testId, questionIdToFind);
    }
  }

  getQuestionIdToFind = (props) => {
    const { questionNumber } = props.match.params;
    const {
      practiceQuestionIds
    } = props.store.practiceTestStore;
    const questionIdToFind = practiceQuestionIds[parseInt(questionNumber, 10) - 1];
    return questionIdToFind;
  }

  @action setQuestionAnswers = (data) => {
    this.questionAnswers = data;
  }
  @observable questionAnswers = {};

  checkNextDisabled = () => {
    const { questionNumber } = this.props.match.params;
    const newQuestionAnswers = _cloneDeep(this.questionAnswers);
    if (!_get(newQuestionAnswers, questionNumber)) {
      return true;
    }
    return false;
  }

  handleAnswerChange = (index, currentQuestion) => {
    const { questionNumber } = this.props.match.params;
    const newQuestionAnswers = _cloneDeep(this.questionAnswers);
    if (!_get(newQuestionAnswers, questionNumber)) {
      newQuestionAnswers[questionNumber] = {
        answers: {},
        questionId: currentQuestion.id
      };
    }

    if (_get(newQuestionAnswers, `${questionNumber}.answers.${index}`)) {
      newQuestionAnswers[questionNumber].answers[index] = false;
    } else {
      newQuestionAnswers[questionNumber].answers[index] = true;
    }

    let isCorrect = true;
    currentQuestion.json_data.answers.map((answer, answerIndex) => {
      if (
        newQuestionAnswers[questionNumber].answers[answerIndex] !== undefined &&
        newQuestionAnswers[questionNumber].answers[answerIndex] !== answer.correct
      ) {
        isCorrect = false;
      }
      return answer;
    });
    newQuestionAnswers[questionNumber].isCorrect = isCorrect;

    this.setQuestionAnswers(newQuestionAnswers);
  }

  handlePrevious = () => {
    this.props.history.goBack();
  }

  handleNext = () => {
    const { testId, testName, questionNumber } = this.props.match.params;
    const {
      practiceQuestionIds
    } = this.props.store.practiceTestStore;
    if (questionNumber < practiceQuestionIds.length) {
      const nextQuestion = parseInt(questionNumber, 10) + 1;
      this.props.history.push(`/tests/${testId}/${testName}/practice/questions/${nextQuestion}`);
    }
  }

  handleSubmitTest = () => {
    const { testId } = this.props.match.params;
    const questionAnswers = _cloneDeep(this.questionAnswers);
    const answeredQuestions = _map(questionAnswers, (val) => {
      const { questionId, isCorrect } = val;
      const answerObj = {
        questionId,
        isCorrect,
        answers: []
      };
      _map(val.answers, (answerBool, key) => {
        if (answerBool) { answerObj.answers.push(key); }
      });
      return answerObj;
    });
    this.props.store.practiceTestStore.postPracticeTest(testId, answeredQuestions);
  }

  render() {
    const { questionNumber } = this.props.match.params;
    const {
      practiceQuestions,
      practiceQuestionIds
    } = this.props.store.practiceTestStore;
    const questionIdToFind = this.getQuestionIdToFind(this.props);
    const currentQuestion = _find(practiceQuestions, (question => {
      return question.id === questionIdToFind;
    }));
    if (_isEmpty(currentQuestion)) {
      return <div>Loading...</div>;
    }
    const { question, answers } = currentQuestion.json_data;

    const answersToRender = answers.map((answer, index) => {
      const key = `${answer.text}-${index}`;
      const questionAnswer = _get(this.questionAnswers, `${questionNumber}.answers.${index}`);
      return (
        <div key={key}>
          <Checkbox
            label={answer.text}
            checked={questionAnswer === true}
            onCheck={() => this.handleAnswerChange(index, currentQuestion)}
          />
        </div>
      );
    });

    const nextButtonDisabled = this.checkNextDisabled();
    const nextButton = questionNumber < practiceQuestionIds.length ? (
      <RaisedButton
        label="Save and go to Next Question"
        primary
        onTouchTap={this.handleNext}
        disabled={nextButtonDisabled}
      />
    ) : (
      <RaisedButton
        label="Finish Test and See Results"
        primary
        onTouchTap={this.handleSubmitTest}
        disabled={nextButtonDisabled}
      />
    );

    return (
      <div className={styles.practiceTestQuestion}>
        <h1>Question {questionNumber} of {practiceQuestionIds.length}</h1>
        <div>
          {currentQuestion.id}: {question}
        </div>
        <div>Check all answers you believe to be correct.</div>
        <div>
          {answersToRender}
        </div>
        <RaisedButton label="Previous Question" primary onTouchTap={this.handlePrevious} />
        {nextButton}
      </div>
    );
  }
}

PracticeTestQuestionContainer.wrappedComponent.propTypes = {
  history: PropTypes.shape().isRequired,
  match: PropTypes.shape().isRequired,
  store: PropTypes.shape().isRequired
};
