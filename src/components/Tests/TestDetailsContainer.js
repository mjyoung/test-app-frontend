import React, { Component, PropTypes } from 'react';
import { action, observable } from 'mobx';
import { inject, observer } from 'mobx-react';
import RaisedButton from 'material-ui/RaisedButton';

import AddQuestionForm from './AddQuestionForm/AddQuestionForm';

import styles from './TestDetailsContainer.scss';

@inject('store') @observer
export default class TestDetailsContainer extends Component {
  componentWillMount() {
    const { params } = this.props.match;
    this.props.store.testStore.fetchSingleTest(params.testId);
  }

  @observable questionFormOpen = false;

  @action handleOpenAddQuestionForm = () => {
    this.questionFormOpen = true;
  }

  @action handleCloseAddQuestionForm = () => {
    this.questionFormOpen = false;
  }

  handleSubmitAddQuestion = (questionFormData) => {
    const { params } = this.props.match;
    this.props.store.questionStore.postQuestionForTest(params.testId, questionFormData);
  }

  handleTakePracticeTest = () => {
    const { selectedTest } = this.props.store.testStore;
    this.props.history.push(`/tests/${selectedTest.id}/${selectedTest.kebabName}/practice`);
  }

  render() {
    const { selectedTest } = this.props.store.testStore;
    if (!selectedTest) {
      return (
        <div>
          Loading...
        </div>
      );
    }

    return (
      <div className={styles.testDetails}>
        <h1>Selected Test: {selectedTest.name}</h1>
        <div>
          <RaisedButton primary label="Take Practice Test" onTouchTap={this.handleTakePracticeTest} />
          <RaisedButton label="Add a Question" onTouchTap={this.handleOpenAddQuestionForm} />
          <AddQuestionForm
            handleClose={this.handleCloseAddQuestionForm}
            handleSubmit={this.handleSubmitAddQuestion}
            isOpen={this.questionFormOpen}
          />
        </div>
      </div>
    );
  }
}

TestDetailsContainer.wrappedComponent.propTypes = {
  history: PropTypes.shape().isRequired,
  match: PropTypes.shape().isRequired,
  store: PropTypes.shape().isRequired
};
