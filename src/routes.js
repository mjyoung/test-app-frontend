import { asyncComponent } from './utils/lazy-load';

import AppStore from './stores/AppStore';
import PracticeTestStore from './stores/PracticeTestStore';
import QuestionStore from './stores/QuestionStore';
import TestStore from './stores/TestStore';
import App from './components/App/App';

const Home = asyncComponent(() => import('./components/Home/Home').then(module => module.default));
const LoginContainer = asyncComponent(() => import('./components/Login/LoginContainer').then(module => module.default));
/* eslint-disable */
const NotFound = asyncComponent(() => import('./components/NotFound').then(module => module.default));
/* eslint-enable */
const PracticeTestContainer = asyncComponent(() => import('./components/PracticeTest/PracticeTestContainer').then(module => module.default));
const PracticeTestQuestionContainer = asyncComponent(() => import('./components/PracticeTest/PracticeTestQuestionContainer').then(module => module.default));
const QuestionDetailsContainer = asyncComponent(() => import('./components/Questions/QuestionDetailsContainer').then(module => module.default));
const RegisterContainer = asyncComponent(() => import('./components/Login/RegisterContainer').then(module => module.default));
const TestsContainer = asyncComponent(() => import('./components/Tests/TestsContainer').then(module => module.default));
const TestDetailsContainer = asyncComponent(() => import('./components/Tests/TestDetailsContainer').then(module => module.default));

const appStore = new AppStore();
const questionStore = new QuestionStore();
const practiceTestStore = new PracticeTestStore();
const testStore = new TestStore();

const Store = {
  appStore, practiceTestStore, questionStore, testStore
};

const routes = [
  {
    component: App,
    store: Store,
    routes: [
      {
        path: '/',
        exact: true,
        component: Home
      },
      {
        path: '/login',
        exact: true,
        component: LoginContainer
      },
      {
        path: '/register',
        exact: true,
        component: RegisterContainer
      },
      {
        path: '/tests',
        exact: true,
        component: TestsContainer
      },
      {
        path: '/tests/:testId/:testName',
        exact: true,
        component: TestDetailsContainer
      },
      {
        path: '/tests/:testId/:testName/questions/:questionId',
        exact: true,
        component: QuestionDetailsContainer
      },
      {
        path: '/tests/:testId/:testName/practice',
        component: PracticeTestContainer,
        routes: [
          {
            path: '/tests/:testId/:testName/practice/questions/:questionNumber',
            exact: true,
            component: PracticeTestQuestionContainer
          }
        ]
      },
      {
        component: NotFound,
        path: '/notfound',
        exact: true
      }
    ]
  }
];

export default routes;
