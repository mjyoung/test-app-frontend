// Needed for Material-UI.
// Keeping in a separate file because of hot reloading issues:
// https://github.com/zilverline/react-tap-event-plugin/issues/61#issuecomment-262566411

import injectTapEventPlugin from 'react-tap-event-plugin';

injectTapEventPlugin();
