import React, { Component, PropTypes } from 'react';
import { renderRoutes } from 'react-router-config';
import { Provider, observer } from 'mobx-react';
import DevTools from 'mobx-react-devtools';

import TopBar from './TopBar/TopBar';
import styles from './App.scss';

@observer
export default class App extends Component {
  constructor(props) {
    super(props);
    this.store = this.props.route.store;
  }

  componentWillMount() {
    this.store.appStore.ping();
  }

  render() {
    const { route } = this.props;
    return (
      <Provider store={this.store}>
        <div className={styles.app}>
          {process.env.NODE_ENV === 'development' && <DevTools />}
          <TopBar />
          <div className={styles.contentWrapper}>
            {renderRoutes(route.routes)}
          </div>
          <footer>
            Test App Footer
          </footer>
        </div>
      </Provider>
    );
  }
}

App.propTypes = {
  route: PropTypes.shape().isRequired
};
