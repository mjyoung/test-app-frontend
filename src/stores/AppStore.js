import { observable, action } from 'mobx';
import axios from '../utils/axios';

const { FB } = window;

const initialUserState = {
  isAuthenticated: false,
  roles: [],
  username: null
};

class AppStore {
  @observable user = initialUserState;

  login = async (email, password) => {
    const { data } = await axios.post('/login', {
      email, password
    });
    if (data) {
      this.setUser({
        isAuthenticated: true,
        roles: data.roles,
        username: data.username
      });
    }
  }

  loginWithFacebook = () => {
    FB.login((response) => {
      if (response.status === 'connected') {
        FB.api('/me', { fields: 'name,email' }, async (response2) => {
          const { data } = await axios.post('/login?type=facebook', {
            email: response2.email,
            fullName: response2.name,
            facebookId: response2.id,
            accessToken: response.authResponse.accessToken
          });
          if (data) {
            this.setUser({
              isAuthenticated: true,
              roles: data.roles,
              username: data.username
            });
          }
        });
      }
    }, {
      scope: 'public_profile,email',
      auth_type: 'rerequest'
    });
  }

  logout = async () => {
    await axios.post('/logout');
    this.setUser(initialUserState);
  }

  ping = async () => {
    const { data } = await axios.get('/ping');
    if (data) {
      this.setUser({
        isAuthenticated: true,
        roles: data.roles,
        username: data.username
      });
    }
  }

  register = async (email, username, password) => {
    const { data } = await axios.post('/register', {
      email, username, password
    });
    if (data) {
      this.setUser({
        isAuthenticated: true,
        roles: data.roles,
        username: data.username
      });
    }
  }

  @action setUser = (user) => {
    this.user = user;
  }
}

export default AppStore;
