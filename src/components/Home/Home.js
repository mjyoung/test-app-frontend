import React, { Component, PropTypes } from 'react';
import { inject, observer } from 'mobx-react';

import styles from './Home.scss';

@inject('store') @observer
export default class Home extends Component {
  render() {
    return (
      <div className={styles.home}>
        <header>
          <h1>Test Smash</h1>
          <h4>
            Blah blah blah content here.
          </h4>
        </header>
        <main>
          <div>
            Blah blah blah content here.
          </div>
        </main>
      </div>
    );
  }
}

Home.wrappedComponent.propTypes = {
  store: PropTypes.shape().isRequired
};
