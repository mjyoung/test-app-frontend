import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router-dom';
import { action, observable } from 'mobx';
import { inject, observer } from 'mobx-react';

import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';

import styles from './RegisterContainer.scss';

@inject('store') @observer
export default class RegisterContainer extends Component {

  @observable email = '';
  @observable username = '';
  @observable password = '';

  @action handleEmailChange = (event, value) => {
    this.email = value;
  }

  @action handleUsernameChange = (event, value) => {
    this.username = value;
  }

  @action handlePasswordChange = (event, value) => {
    this.password = value;
  }

  handleRegister = () => {
    this.props.store.appStore.register(this.email, this.username, this.password);
  }

  render() {
    return (
      <div className={styles.register}>
        <TextField
          value={this.email}
          floatingLabelText="Email"
          onChange={this.handleEmailChange}
        />
        <TextField
          value={this.username}
          floatingLabelText="Username (Optional)"
          onChange={this.handleUsernameChange}
        />
        <TextField
          value={this.password}
          floatingLabelText="Password"
          type="password"
          onChange={this.handlePasswordChange}
        />
        <RaisedButton
          className={styles.registerButton}
          label="Submit"
          primary
          onTouchTap={this.handleRegister}
        />
        <div>
          Already registered? <Link to="/login">Log in</Link> now!
        </div>
      </div>
    );
  }
}

RegisterContainer.wrappedComponent.propTypes = {
  store: PropTypes.shape().isRequired
};
