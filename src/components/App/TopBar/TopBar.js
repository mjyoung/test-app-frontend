import React, { Component, PropTypes } from 'react';
import { inject, observer } from 'mobx-react';
import { Link } from 'react-router-dom';

import styles from './TopBar.scss';

@inject('store') @observer
export default class TopBar extends Component {
  handleLogout = () => {
    this.props.store.appStore.logout();
  }

  render() {
    const { user } = this.props.store.appStore;
    return (
      <div className={styles.topBar}>
        <nav>
          <div>
            <Link to="/">Home</Link>
            <Link to="/tests">Tests</Link>
          </div>
          <div>
            {user.isAuthenticated ?
              <button onClick={this.handleLogout}>{user.username} (Logout)</button> :
              <Link to="/login">Login</Link>
            }
          </div>

        </nav>
      </div>
    );
  }
}

TopBar.wrappedComponent.propTypes = {
  store: PropTypes.shape().isRequired
};
