import React, { Component, PropTypes } from 'react';
import { action, observable } from 'mobx';
import { inject, observer } from 'mobx-react';

import RaisedButton from 'material-ui/RaisedButton';

import Vote from '../Vote/Vote';

import styles from './QuestionDetailsContainer.scss';

@inject('store') @observer
export default class QuestionDetailsContainer extends Component {
  componentWillMount() {
    const { params: {
      testId, questionId
    } } = this.props.match;
    this.props.store.questionStore.fetchSingleQuestionForTest(testId, questionId);
  }

  @observable viewSolution = false;

  @action toggleViewSolution = () => {
    this.viewSolution = !this.viewSolution;
  }

  handleSubmitVote = (vote) => {
    const {
      params: {
        testId, questionId
      }
    } = this.props.match;
    this.props.store.questionStore.postVoteForQuestion(testId, questionId, vote);
  }

  render() {
    const { selectedQuestion } = this.props.store.questionStore;
    if (!selectedQuestion) {
      return (
        <div>
          Loading...
        </div>
      );
    }

    const answersToRender = selectedQuestion.json_data.answers.map((answer) => {
      let answerClass = answer.correct ? styles.correctAnswer : styles.wrongAnswer;
      if (this.viewSolution) {
        answerClass += ` ${styles.viewSolution}`;
      }
      return <div key={answer.text} className={answerClass}>{answer.text}</div>;
    });

    return (
      <div className={styles.questionDetails}>
        <Vote
          currentScore={selectedQuestion.vote_count}
          upvote={() => this.handleSubmitVote('upvote')}
          downvote={() => this.handleSubmitVote('downvote')}
        />
        <h1>Question: {selectedQuestion.test.name}</h1>
        <div>{selectedQuestion.json_data.question}</div>
        <div>
          {answersToRender}
        </div>
        <RaisedButton
          primary
          label={this.viewSolution ? 'Hide Solution' : 'View Solution'}
          onTouchTap={this.toggleViewSolution}
        />
      </div>
    );
  }
}

QuestionDetailsContainer.wrappedComponent.propTypes = {
  match: PropTypes.shape().isRequired,
  store: PropTypes.shape().isRequired
};
