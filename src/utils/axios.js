import axios from 'axios';

axios.defaults.baseURL = process.env.API_ROOT_URL || process.env.STORYBOOK_API_ROOT_URL;
axios.defaults.withCredentials = true;

export default axios;
